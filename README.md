# usegalaxy.eu infrastructure playbook

Ansible playbook for managing UseGalaxy IMPaCT-Data infrastructure.

## Detailed documentation
* Is available [here](https://impact-data-ref-imp.readthedocs.io/es/latest/content/ref-imp/components/central/galaxy-server.html)

# Licencia

[Academic Free License ("AFL") v. 3.0][afl]

[afl]: http://opensource.org/licenses/AFL-3.0

# Agradecimientos

_El proyecto IMPaCT-Data (Exp. IMP/00019) ha sido financiado por el Instituto de Salud Carlos III, co-financiado por el Fondo Europeo de Desarrollo Regional (FEDER, “Una manera de hacer Europa”)._


